set nocompatible
syntax enable 
filetype plugin indent on

set nobackup
set noswapfile 

set mouse=a 
set shiftwidth=4  " one tab == four spaces 
set tabstop=4     " one tab == four spaces

set backspace=indent,eol,start

set showmatch 

" change leader key 
let mapleader = ',' 

set number 

set ruler 

call plug#begin(expand('~/.vim/plugged'))
Plug 'arcticicestudio/nord-vim'
call plug#end() 

colorscheme nord 
