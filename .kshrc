
/usr/local/bin/neofetch

alias ls="ls -a"
alias config="/usr/local/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME"
alias cl='clear'
alias sd='doas shutdown -p now'
alias rb='doas reboot'
alias ..='cd ..'
alias ...='cd ../..'
